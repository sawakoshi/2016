# -*- coding: utf-8 -*-

"""辞書(Dict_...)とレシピの材料のマッチングを行う"""

def Read_mate():    """レシピから(材料,分量)のリストを作成"""
    f = open("recipedata100-1","r")
    mate = []
    for line in f:
        m = []
        for shok in line.split("<>"):
            if shok != "\n":
                m.append(shok)
        mate.append(m)
    f.close()
    return mate


def make_dict(Dict):    """辞書から読み込み"""
   f = open(Dict,"r")
   m = []
   for line in f:
       m.append(line.split(","))
   f.close()
   return m


def Match_mate(mate):    """辞書とレシピの材料のマッチング"""
    Dict = make_dict("Dict_FoodGroup")
    flag = 0
    count = 0
    ok = []  """マッチングした材料のリスト"""
    no = []  """マッチングしなかった材料のリスト"""
    for m in mate:
        for shok in m:
            j = shok.split(",")[0]
            for shok_dict in Dict:
                if j != "" and j == shok_dict[0] or j == shok_dict[1] or j == shok_dict[2]:
                    flag = 1
            if flag == 1:
                if not j in ok:
                    ok.append(j)
                    """count += 1"""
            elif flag == 0:
                if not j in ok:
                    no.append(j)
                    count += 1
            flag = 0
    print count
    return no

kk = Read_mate()
no = Match_mate(kk)

for n in no:
    print n

