# -*- coding: utf-8 -*-
import unicodedata

"""レシピから食材リストを作成"""
def Read_mate(recipe):
    f = open(recipe,"r")
    matelist = []
    for line in f:
        mate = []
        survfor = line.split("/")[0]
        try:
            line = line.split("/")[1]
        except:
            pass
        for shok in line.split("<>"):
            if shok != "\n":
                mate.append(shok)
        matelist.append([survfor, mate])
    f.close()
    return matelist

"""食材リストと食品グラム変換辞書を用いて分量をgに変換する"""
def Read_gram(matelist, dict_ID):
    re_matelist = []
    for mate in matelist:
        shok = mate.split(",")[0]
        quan = mate.split(",")[1]
        flag_gram = check_gram(quan)
        flag_cup = check_cup(quan)
        if flag_cup != 0:
            try:
                quan = trans_gramcup(shok, flag_cup, dict_ID)
                re_mate = [shok,quan]
                re_matelist.append(re_mate)
            except:
                pass
        elif flag_gram != 0:
            re_mate = [shok,flag_gram]
            re_matelist.append(re_mate)
        else:
            try:
                kon = Match_gram(shok, dict_ID)
                unit = kon[0]
                gram = kon[1]
                quan = trans_gram(quan, unit, gram)
                re_mate = [shok, quan]
                re_matelist.append(re_mate)
            except:
                pass
    return re_matelist


"""食材ID辞書と食品グラム変換辞書を用いて食材の単位とそのgを取り出す"""
def Match_gram(shok,dict_ID):
    try:
        ID = Read_ID(shok, dict_ID)
    except:
        print shok + "は辞書に登録されていません\n"
        return 0
    dict_gram = make_dict("Dict_Foodgram")
    if ID in dict_gram:
        unit = dict_gram[ID][0]
        gram = int(dict_gram[ID][1])
        return [unit, gram]
    else:
        return 0


"""分量をgに変換する"""
def trans_gram(quan, unit, gram):
    if quan.find(unit) == -1:
        print "単位が異なります\n"
        return 0
    else:
        quan = quan.split(unit)[0]
        quan = unicodedata.normalize('NFKC', quan.decode("utf-8"))
        quan = quan.encode("utf-8")
        if quan.find("/") == -1:
            return int(quan) * int(gram)
        else:
            return (float(quan.split("/")[0]) / float(quan.split("/")[1])) * int(gram)


"""大さじ小さじカップ表記の分量をグラムに変換する"""
def trans_gramcup(shok, flag_cup, dict_ID):
    try:
        ID = Read_ID(shok, dict_ID)
    except:
        print shok + "は辞書に登録されていません\n"
        return 0
    dict_gramcup = make_dict("Dict_Foodgram_cup")
    if ID in dict_gramcup:
        if flag_cup[0] == "大":
            gram = int(dict_gramcup[ID][3])
        elif flag_cup[0] == "小":
            gram = int(dict_gramcup[ID][2])
        elif flag_cup[0] == "カップ":
            gram = int(dict_gramcup[ID][4])
        return int(flag_cup[1]) * gram
    else:
        return 0


"""辞書作成"""
def make_dict(Dict):
   f = open(Dict,"r")
   m = {}
   for line in f:
       cut = line.split(",")
       m[cut[0]] = cut
   f.close()
   return m


"""食材のIDを登録した辞書dict_IDを作成"""
def make_ID():
    f = open("Dict_FoodID","r")
    dict_ID = {}
    for line in f:
        cut = line.split(",")
        ID = cut[0]
        for i in cut:
            dict_ID[i] = ID
    f.close()
    return dict_ID


"""辞書dict_IDを用いてshok(食材)のIDを検索"""
def Read_ID(shok, dict_ID):
    return dict_ID[shok]


"""分量が◯◯gと表記されているか確認。書かれていた場合は◯◯の部分を取り出す"""
def check_gram(quan):
    if quan.endswith("g"):
        quan = unicodedata.normalize('NFKC', quan.split("g")[0].decode("utf-8"))
        return quan.encode("utf-8")
    elif quan.endswith("ｇ"):
        quan = unicodedata.normalize('NFKC', quan.split("ｇ")[0].decode("utf-8"))
        return quan.encode("utf-8")
    elif quan.endswith("グラム"):
        quan = unicodedata.normalize('NFKC', quan.split("グラム")[0].decode("utf-8"))
        return quan.encode("utf-8")
    else:
        return 0


"""分量が大さじ小さじカップでかかれているか確認。"""
def check_cup(quan):
    if quan.startswith("大さじ"):
        quan = unicodedata.normalize('NFKC', quan.split("大さじ")[1].decode("utf-8"))
        return ["大", quan.encode("utf-8")]
    elif quan.startswith("大"):
        quan = unicodedata.normalize('NFKC', quan.split("大")[1].decode("utf-8"))
        return ["大", quan.encode("utf-8")]
    elif quan.startswith("小さじ"):
        quan = unicodedata.normalize('NFKC', quan.split("小さじ")[1].decode("utf-8"))
        return ["小", quan.encode("utf-8")]
    elif quan.startswith("小"):
        quan = unicodedata.normalize('NFKC', quan.split("小")[1].decode("utf-8"))
        return ["小", quan.encode("utf-8")]
    elif quan.endswith("カップ"):
        quan = unicodedata.normalize('NFKC', quan.split("カップ")[1].decode("utf-8"))
        return ["カップ", quan.encode("utf-8")]
    else:
        return 0


"""matelist(食材リスト)と食品群辞書を用いて分量を点数化、食品群を追加"""
def Read_group(matelist, dict_ID):
    re_matelist = []
    for mate in matelist:
        shok = mate[0]
        quan = mate[1]
        try:
            kon = Match_group(shok, dict_ID)
            group = kon[0]
            reject = kon[1]
            cal = kon[2]
            quan = trans_use(quan, reject, cal)
            re_mate = [shok,quan,group]
            re_matelist.append(re_mate)
        except:
            pass
    return re_matelist


"""食材、食材ID辞書、食品群辞書を用いて食品の群、廃棄率、1点あたりのcalを取り出す"""
def Match_group(shok, dict_ID):
    try:
        ID = Read_ID(shok, dict_ID)
    except:
        #print shok + "は辞書に登録されていません\n"
        return 0
    dict_group = make_dict("Dict_FoodGroup")
    if ID in dict_group:
        group = dict_group[ID][4]
        reject = int(dict_group[ID][2])
        cal = int(dict_group[ID][3])
        return [group,reject,cal]
    else:
        return 0


"""4群点数法の点数化"""
def trans_use(quan, reject, cal):
    return (float(quan)*((100-reject)/100))/cal


"""食材リストから指定された類に分類し点数を加算していく"""
def Read_value(matelist, survfor):
    value = {"乳類":0, "卵類":0, "魚介類":0, "肉類":0, "豆類":0, "野菜類":0,
             "いも類":0, "果実類":0, "穀類":0, "砂糖類":0, "油脂類":0}
    for mate in matelist:
        value[mate[2]] = value[mate[2]] + float(mate[1])/survfor
    return value


"""何人分か確認"""
def Read_survfor(survfor):
    if survfor.endswith("人分"):
        survfor = unicodedata.normalize('NFKC', survfor.split("人分")[0].decode("utf-8"))
        survfor = survfor.encode("utf-8")
    elif survfor.endswith("人前"):
        survfor = unicodedata.normalize('NFKC', survfor.split("人前")[0].decode("utf-8"))
        survfor = survfor.encode("utf-8")
    else:
        return 1
    if survfor.find("~") == -1:
        return float(survfor)
    else:
        return (float(survfor.split("~")[0]) + float(survfor.split("~")[1])) / 2
 

dict_ID = make_ID()
matelist = Read_mate("recipi_sample")
re_matelist = matelist[0][1]
survfor = Read_survfor(matelist[0][0])

print "レシピ"
for i in re_matelist:
    print i

print "\n"

re_matelist = Read_gram(re_matelist, dict_ID)

re_matelist = Read_group(re_matelist, dict_ID)

for i in re_matelist:
    print i[0], i[1], i[2]

value = Read_value(re_matelist, survfor)
print value
