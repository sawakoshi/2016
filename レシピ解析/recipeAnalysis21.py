# -*- coding: utf-8 -*-
import unicodedata
import MeCab
import math
import sys
import re

mecab = MeCab.Tagger("Ochasen")

"""レシピから食材リストを作成"""
def Read_mate(recipe):
    f = open(recipe,"r")
    matelist = []
    for line in f:
        mate = []
        survfor = line.split("/")[0]
        try:
            title = line.split("/")[0]
            survfor = line.split("/")[1]
            line = line.split("/")[2]
        except:
            title = "なし"
            survfor = "なし"
        for shok in line.split("<>"):
            if shok != "\n":
                mate.append(shok)
        matelist.append([title, survfor, mate])
    f.close()
    return matelist


def Macth_ID(matelist, dict_ID):
    re_matelist = []
    count = 0
    count_ok = 0
    count_ng = 0
    count_ngok = 0
    for mate in matelist:
        shok = mate.split(",")[0]
        #print "材料名：", shok
        try:
            quan = mate.split(",")[1]
        except:
            quan = "0"
        count += 1
        if shok.find("（") != -1:
            shok = shok.split("（")[0]
        elif shok.find("(") != -1:
            shok = shok.split("(")[0]
        ID = Read_ID(shok, dict_ID)
        if len(ID) == 0:
            classify = {}
            count_ng += 1
            node = mecab.parseToNode(shok)
            while node:
                meta = node.feature.split(",")
                word = node.surface
                ID = Read_ID(word, dict_ID)
                if meta[0] == "名詞":
                    if len(ID) != 0:
                        for i in ID:
                            if not i in classify:
                                classify[i] = 1
                            else:
                                classify[i] += 1
                node = node.next
            try:
                ID = max([(v,k) for k,v in classify.items()])[1]
                shok = Search_ID(ID)
                count_ngok += 1
            except:
                pass
        else:
            classify = {}
            for i in ID:
                if not i in classify:
                    classify[i] = 1
                else:
                    classify[i] += 1
            ID = max([(v,k) for k,v in classify.items()])[1]
            shok = Search_ID(ID)
            count_ok += 1
        if len(Read_ID(shok, dict_ID)) != 0:
            #print "特定した食材：", shok
            re_matelist.append(ID + "," + quan)
    return re_matelist, [count, count_ok, count_ng, count_ngok]

 


"""食材リストと食品グラム変換辞書を用いて分量をgに変換する"""
def Read_gram(matelist, dict_ID):
    re_matelist = []
    for mate in matelist:
        ID = mate.split(",")[0]
        try:
            quan = mate.split(",")[1]
        except:
            quan = "0"
        #print "変換前：", Search_ID(ID), quan
        flag_gram = check_gram(quan)
        flag_cup = check_cup(quan)
        if flag_cup != 0:
            try:
                quan = trans_gramcup(ID, flag_cup, dict_ID)
                #print "変換後：", Search_ID(ID), quan
                re_mate = [ID,quan]
                re_matelist.append(re_mate)
            except:
                pass
        elif flag_gram != 0:
            #print "変換後：", Search_ID(ID), flag_gram
            re_mate = [ID,flag_gram]
            re_matelist.append(re_mate)
        else:
            try:
                kon = Match_gram(ID, dict_ID)
                unit = kon[0]
                gram = kon[1]
                print "変換前：", Search_ID(ID), quan
                quan = trans_gram(quan, unit, gram)
                print "変換後：", Search_ID(ID), quan
                re_mate = [ID, quan]
                re_matelist.append(re_mate)
            except:
                pass
    return re_matelist


"""食材ID辞書と食品グラム変換辞書を用いて食材の単位とそのgを取り出す"""
def Match_gram(ID,dict_ID):
    if ID == -1:
        #print shok + "は辞書に登録されていません\n"
        return 0
    dict_gram = make_dict("Dict_Foodgram")
    if ID in dict_gram:
        unit = dict_gram[ID][2]
        gram = int(dict_gram[ID][3])
        return [unit, gram]
    else:
        return 0


"""分量をgに変換する"""
def trans_gram(quan, unit, gram):
    if quan.find(unit) == -1:
        return 0
    else:
        quan = quan.split(unit)[0]
        quan = unicodedata.normalize('NFKC', quan.decode("utf-8"))
        quan = quan.encode("utf-8")
        if quan.find("/") == -1:
            suji = re.match("\d*",quan)
            quan = suji.group()
            return int(quan) * int(gram)
        else:
            return (float(quan.split("/")[0]) / float(quan.split("/")[1])) * int(gram)


"""大さじ小さじカップ表記の分量をグラムに変換する"""
def trans_gramcup(ID, flag_cup, dict_ID):
    if ID == -1:
        #print shok + "は辞書に登録されていません\n"
        return 0
    dict_gramcup = make_dict("Dict_Foodgram_cup")
    if ID in dict_gramcup:
        if flag_cup[0] == "大":
            gram = int(dict_gramcup[ID][3])
        elif flag_cup[0] == "小":
            gram = int(dict_gramcup[ID][2])
        elif flag_cup[0] == "カップ":
            gram = int(dict_gramcup[ID][4])
        return int(flag_cup[1]) * gram
    else:
        return 0


"""辞書作成"""
def make_dict(Dict):
   f = open(Dict,"r")
   m = {}
   for line in f:
       cut = line.split(",")
       m[cut[0]] = cut
   f.close()
   return m


"""食材のIDを登録した辞書dict_IDを作成"""
def make_ID():
    f = open("Dict_FoodID_2","r")
    dict_ID = []
    for line in f:
        ID = []
        cut = line.split(",")
        for i in cut:
            if i.endswith("\n"):
                i = i.split("\n")[0]
            ID.append(i)
        dict_ID.append(ID)
    f.close()
    return dict_ID


"""辞書dict_IDを用いてshok(食材)のIDを検索"""
def Read_ID(shok, dict_ID):
    ret = []
    for i in dict_ID:
        for j in i:
            if shok == j:
                ret.append(i[0])
    return ret

def Search_ID(ID):
    f = open("Dict_FoodID_2","r")
    for line in f:
        if ID == line.split(",")[0]:
            return line.split(",")[1]

"""分量が◯◯gと表記されているか確認。書かれていた場合は◯◯の部分を取り出す"""
def check_gram(quan):
    if quan.find("g") != -1:
        quan = unicodedata.normalize('NFKC', quan.split("g")[0].decode("utf-8"))
        quan = quan.encode("utf-8")
        suji = re.match("\d*",quan)
        quan = suji.group()
    elif quan.find("ｇ") != -1:
        quan = unicodedata.normalize('NFKC', quan.split("ｇ")[0].decode("utf-8"))
        quan = quan.encode("utf-8")
        suji = re.match("\d*",quan)
        quan = suji.group()
    elif quan.find("グラム") != -1:
        quan = unicodedata.normalize('NFKC', quan.split("グラム")[0].decode("utf-8"))
        quan = quan.encode("utf-8")
        suji = re.match("\d*",quan)
        quan = suji.group()
    else:
        return 0
    if quan.find("~") == -1:
        return quan
    else:
        return (float(quan.split("~")[0]) + float(quan.split("~")[1])) / 2


"""分量が大さじ小さじカップでかかれているか確認。"""
def check_cup(quan):
    if quan.startswith("大さじ"):
        quan = unicodedata.normalize('NFKC', quan.split("大さじ")[1].decode("utf-8"))
        quan = quan.encode("utf-8")
        suji = re.match("\d*",quan)
        quan = suji.group()
        return ["大", quan]
    elif quan.startswith("大"):
        quan = unicodedata.normalize('NFKC', quan.split("大")[1].decode("utf-8"))
        quan = quan.encode("utf-8")
        suji = re.match("\d*",quan)
        quan = suji.group()
        return ["大", quan]
    elif quan.startswith("小さじ"):
        quan = unicodedata.normalize('NFKC', quan.split("小さじ")[1].decode("utf-8"))
        quan = quan.encode("utf-8")
        suji = re.match("\d*",quan)
        quan = suji.group()
        return ["小", quan]
    elif quan.startswith("小"):
        quan = unicodedata.normalize('NFKC', quan.split("小")[1].decode("utf-8"))
        quan = quan.encode("utf-8")
        suji = re.match("\d*",quan)
        quan = suji.group()
        return ["小", quan]
    elif quan.endswith("カップ"):
        quan = unicodedata.normalize('NFKC', quan.split("カップ")[1].decode("utf-8"))
        quan = quan.encode("utf-8")
        suji = re.match("\d*",quan)
        quan = suji.group()
        return ["カップ", quan]
    else:
        return 0


"""matelist(食材リスト)と食品群辞書を用いて分量を点数化、食品群を追加"""
def Read_group(matelist, dict_ID):
    re_matelist = []
    for mate in matelist:
        ID = mate[0]
        quan = mate[1]
        try:
            kon = Match_group(ID, dict_ID)
            group = kon[0]
            reject = kon[1]
            cal = kon[2]
            quan = trans_use(quan, reject, cal)
            re_mate = [ID,quan,group]
            re_matelist.append(re_mate)
        except:
            pass
    return re_matelist


"""食材、食材ID辞書、食品群辞書を用いて食品の群、廃棄率、1点あたりのcalを取り出す"""
def Match_group(ID, dict_ID):
    if ID == -1:
        #print shok + "は辞書に登録されていません\n"
        return 0
    dict_group = make_dict("Dict_FoodGroup")
    if ID in dict_group:
        group = dict_group[ID][4]
        reject = float(dict_group[ID][2])
        cal = float(dict_group[ID][3])
        return [group,reject,cal]
    else:
        return 0


"""4群点数法の点数化"""
def trans_use(quan, reject, cal):
    return (float(quan)*((100-reject)/100))/cal


"""食材リストから指定された類に分類し点数を加算していく"""
def Read_value(matelist, survfor):
    value = {"乳類":0, "卵類":0, "魚介類":0, "肉類":0, "豆類":0, "野菜類":0,
             "いも類":0, "果実類":0, "穀類":0, "砂糖類":0, "油脂類":0}
    for mate in matelist:
        value[mate[2]] = value[mate[2]] + float(mate[1])/survfor
    return value


def Trans_group(value):
    re_value = {}
    re_value["第1群"] = value["乳類"] + value["卵類"]
    re_value["第2群"] = value["魚介類"] + value["肉類"] + value["豆類"]
    re_value["第3群"] = value["野菜類"] + value["いも類"] + value["果実類"]
    re_value["第4群"] = value["穀類"] + value["砂糖類"] + value["油脂類"]
    return re_value


"""何人分か確認"""
def Read_survfor(survfor):
    try:
        if survfor.endswith("人分"):
            survfor = unicodedata.normalize('NFKC', survfor.split("人分")[0].decode("utf-8"))
            survfor = survfor.encode("utf-8")
        elif survfor.endswith("人前"):
            survfor = unicodedata.normalize('NFKC', survfor.split("人前")[0].decode("utf-8"))
            survfor = survfor.encode("utf-8")
        else:
            return 3.5
        if survfor.find("~") == -1:
            return float(survfor)
        else:
            return (float(survfor.split("~")[0]) + float(survfor.split("~")[1])) / 2
    except:
        return 3.5

 
def Min_composition(value):
    return min(value, key=(lambda x: value[x]))


def Max_composition(value_sub, Min_composition):
    Max = 0
    Max_value = {}
    for i in value_sub:
        if i[Min_composition] > Max:
            Max = i[Min_composition]
            Max_value = i
    return Max_value


def valueplus(value, Max_value):
    value_ret = {}
    for i in Max_value.keys():
        value_ret[i] = value[i] + Max_value[i]
    return value_ret

def cast_kanrendo(value, value_sub):
    kanrendo = 0
    for title, mate in value_sub:
        value2 = valueplus(value, mate)
        mm = cast_group("乳類", value2) + cast_group("卵類", value2) + cast_group("豆類", value2)
        + cast_group("野菜類", value2) + cast_group("いも類", value2) + cast_group("果実類", value2)
        + cast_group("肉・魚介類", value2)
        if value2["乳類"] > 4 or value2["卵類"] > 2 or value2["豆類"] > 2 or value2["野菜類"] > 2 or value2["いも類"] > 2 or value2["果実類"] > 2 or value2["肉類"] + value2["魚介類"] > 6:
            pass
        elif mm > kanrendo:
            kanrendo = mm
            valueaccept = mate
            titleaccept = title
    return titleaccept, valueaccept, kanrendo

def cast_group(group, value):
    if group =="肉・魚介類":
        ret = float(value["肉類"]) + float(value["魚介類"])
        return math.fabs(float(2 - math.fabs(2 - ret)))/2
    elif group == "乳類":
        return math.fabs(float(2 - math.fabs(2 - value[group])))/2
    elif group == "卵類" or "豆類" or "野菜類" or "いも類" or "果実類":
        return math.fabs(float(1 - math.fabs(1 - value[group])))/1
       

def cast_kanrendo22(value, value_sub):
    kanrendo = 0
    for title, mate in value_sub:
        value2 = valueplus(value, mate)
        mm = math.fabs(float(3 - math.fabs(3 - value2["第1群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第2群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第3群"])))/3
        if mm > kanrendo:
            kanrendo = mm
            valueaccept = mate
            titleaccept = title
    return titleaccept, valueaccept, kanrendo



def print_value(value):
    print "乳類：", value["乳類"]
    print "卵類：", value["卵類"]
    print "肉・魚介類：", value["肉類"] + value["魚介類"]
    print "豆類：", value["豆類"]
    print "野菜類：", value["野菜類"]
    print "いも類：", value["いも類"]
    print "果実類：", value["果実類"] 
    return


matelist = Read_mate("recipedata_main1")

a = input("主菜を決めて下さい：")
num = int(a) - 1

dict_ID = make_ID()
matelist_sub = Read_mate("recipedata_sub1")

re_matelist = matelist[num][2]
survfor = Read_survfor(matelist[num][1])
re_matelist, b = Macth_ID(re_matelist, dict_ID)
re_matelist = Read_gram(re_matelist, dict_ID)
re_matelist = Read_group(re_matelist, dict_ID)

for i in re_matelist:
    print Search_ID(i[0])
    print i[2] + "：" + str(i[1])
value = Read_value(re_matelist, survfor)

value_sub = []
count = 0
count_ok = 0
count_ng = 0
count_ngok = 0
for k in matelist_sub:
    re_matelist_sub = k[2]
    survfor_sub = Read_survfor(k[1])
    re_matelist_sub, counter = Macth_ID(re_matelist_sub, dict_ID)
    re_matelist_sub = Read_gram(re_matelist_sub, dict_ID)
    re_matelist_sub = Read_group(re_matelist_sub, dict_ID)
    valuesub = Read_value(re_matelist_sub, survfor_sub)
    value_sub.append([k[0], valuesub])
    count = count + counter[0]
    count_ok = count_ok + counter[1]
    count_ng = count_ng + counter[2]
    count_ngok = count_ngok + counter[3]

print "全食材：", count
print "IDに登録されていた食材：", count_ok
print "登録されていなかった食材：", count_ng
print "形態素解析で見つかった食材：", count_ngok

print "選ばれたレシピ：" + matelist[num][0]
print "主菜"
print_value(value)

flag = sys.argv[1]
if flag == "1":
    titleaccept, valueaccept, kanrendo = cast_kanrendo(value, value_sub)
elif flag == "2":
    titleaccept, valueaccept, kanrendo = cast_kanrendo22(value, value_sub)


print "\n関連度：", kanrendo
print "\n関連度がもっとも高いレシピ：", titleaccept
print_value(valueaccept)

print "\n合計した栄養バランス"
value = valueplus(value, valueaccept)
print_value(value)



if flag == "1":
    titleaccept, valueaccept, kanrendo = cast_kanrendo(value, value_sub)
elif flag == "2":
    titleaccept, valueaccept, kanrendo = cast_kanrendo22(value, value_sub)


print "\n関連度：", kanrendo
print "\n関連度がもっとも高いレシピ：", titleaccept
print_value(valueaccept)

print "\n合計した栄養バランス"
value = valueplus(value, valueaccept)
print_value(value)

