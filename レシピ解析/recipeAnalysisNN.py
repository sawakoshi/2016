# -*- coding: utf-8 -*-
import unicodedata
import MeCab
import math
import sys
import re

#下の実行部分か見たほうが分かりやすいと思います（400行から）

mecab = MeCab.Tagger("Ochasen")

"""レシピから食材リストを作成"""
def Read_mate(recipe):
    f = open(recipe,"r")
    matelist = []
    for line in f:
        mate = []
        try:
            title = line.split("/", 2)[0]#title:レシピのタイトル
            survfor = line.split("/", 2)[1]#survfor:何人分
            line = line.split("/", 2)[2]#line:食材,分量<>食材,分量<>食材,分量<>...(１つのlineで１レシピ分）
        except:
            title = "なし"
            survfor = "なし"
        for shok in line.split("<>"):
            if shok != "\n":
                mate.append(shok)#mate:["食材,分量","食材,分量",,,,](１つのmateで１レシピ分）
        matelist.append([title, survfor, mate])
    f.close()
    return matelist#matelist:[[１レシピ分のデータ],[１レシピ分のデータ],,,,]

"""食品ID辞書を用いて食材特定"""
def Macth_ID(matelist, dict_ID):#matelist:["食材,分量","食材,分量",,,]
    re_matelist = []
    for mate in matelist:
        shok = mate.split(",")[0]#shok:食材名
        try:
            quan = mate.split(",")[1]#quan:分量
        except:
            quan = "0"
        if shok.find("（") != -1:#食材名に（があればそれ以降をカット
            shok = shok.split("（")[0]
        elif shok.find("(") != -1:
            shok = shok.split("(")[0]
        ID = Read_ID(shok, dict_ID)#食材名から食品IDを検索
        if len(ID) == 0:#IDの長さが０ということは食品IDが見つからなかったということ
            classify = {}
            node = mecab.parseToNode(shok)#形態素解析します（形態素解析については各自勉強してください）
            while node:
                meta = node.feature.split(",")
                word = node.surface
                ID = Read_ID(word, dict_ID)
                if meta[0] == "名詞":#名詞の形態素だけ仕様
                    if len(ID) != 0:
                        for i in ID:#i:食品ID
                            if not i in classify:#食品IDをclassifyに格納していく同じIDの場合は+1する
                                classify[i] = 1
                            else:
                                classify[i] += 1
                node = node.next
            try:
                ID = max([(v,k) for k,v in classify.items()])[1]#classifyの中で一番値の大きい（一番出現回数が多い）食品IDをその食材のIDとする
                shok = Search_ID(ID)#IDから食材名を検索
            except:
                pass
        else:#食材名そのままで食品IDが見つかった場合
            classify = {}
            for i in ID:
                if not i in classify:
                    classify[i] = 1
                else:
                    classify[i] += 1
            ID = max([(v,k) for k,v in classify.items()])[1]
            shok = Search_ID(ID)
        if len(Read_ID(shok, dict_ID)) != 0:
            re_matelist.append(ID + "," + quan)
        else:
            pass
    return re_matelist#re_matelist:[[食品ID,分量],[食品ID,分量],,,]

 


"""食材リストと食品グラム変換辞書を用いて分量をgに変換する"""
def Read_gram(matelist, dict_ID):#matelist:[[食品ID,分量],[食品ID,分量],,,,]
    re_matelist = []
    for mate in matelist:
        ID = mate.split(",")[0]
        try:
            quan = mate.split(",")[1]
        except:
            quan = "0"
        flag_gram = check_gram(quan)#グラム表記か確認、同時に数字を取り出す
        flag_cup = check_cup(ID, quan)#カップ表記か確認、同時に変換
        if flag_cup != 0:
           try:
                re_mate = [ID,flag_cup]
                re_matelist.append(re_mate)
           except:
                pass
        elif flag_gram != 0:
            re_mate = [ID,flag_gram]
            re_matelist.append(re_mate)
        else:#グラム表記でもカップ表記でもなければ単位表記であると判断して処理する
            try:
                kon = Match_gram(ID, dict_ID)#単位グラム変換辞書からIDが指す食材の単位とグラムを取り出す
                if kon == 0:
                    quan = 0
                else:
                    unit = kon[0]#unit:単位
                    gram = kon[1]#gram:1単位あたりのグラム
                    quan = trans_gram(quan, unit, gram)#グラムに変換する
                re_mate = [ID, quan]
                re_matelist.append(re_mate)
            except:
                pass
    return re_matelist#re_matelist:[[食品ID,グラム],[食品ID,グラム],,,,]


"""分量が◯◯gと表記されているか確認。書かれていた場合は◯◯の部分を取り出す"""
def check_gram(quan):
    if quan.find("g") != -1 or quan.find("ｇ") != -1 or quan.find("グラム") != -1:
        quan = unicodedata.normalize('NFKC', quan.decode("utf-8"))#すべてを半角に変換
        quan = quan.encode("utf-8")
    else:
        return 0
    if quan.find("~") == -1:
        suji = re.search("\d+",quan)#数字部分だけ取り出す（正規表現）
        quan = suji.group()
        return quan
    else:#分量に～がある場合１～２個という表記になるので真ん中の1.5を分量とする
        quan = quan.split("~")
        suji = re.search("\d+",quan[0])
        quan[0] = suji.group()           
        suji = re.search("\d+",quan[1])
        try:
            quan[1] = suji.group()
        except:
            return float(quan[0])
        try:
            return (float(quan[0]) + float(quan[1])) / 2
        except:
            return 0


"""分量が大さじ小さじカップでかかれているか確認。"""
def check_cup(ID, quan):
    dict_gramcup = make_dict("Dict_Foodgram_cup")
    if ID in dict_gramcup:
        if quan.startswith("大さじ") or quan.startswith("大匙") or quan.startswith("大"):
            gram = float(dict_gramcup[ID][3])
        elif quan.startswith("小さじ") or quan.startswith("小匙") or quan.startswith("小"):
            gram = float(dict_gramcup[ID][2])
        elif quan.find("マグカップ") != -1:#マグカップ1杯分とか書いてたレシピがあったので、普通はいりません
            return 0
        elif quan.find("カップ") != -1:
            gram = float(dict_gramcup[ID][4])
        elif quan.find("ml") != -1 or quan.find("cc") != -1:#200ml,200ccで1カップ
            gram = float(dict_gramcup[ID][4]) / 200
        else:
            return 0
    else:
        return 0
    quan = unicodedata.normalize('NFKC', quan.decode("utf-8"))
    quan = quan.encode("utf-8")
    if quan.find("/") == -1:#分量に/がある場合1/2という表記になるので0.5とする
        if quan.find("~") == -1:
            suji = re.search("\d+",quan)
            try:
                quan = suji.group()
                return  float(quan) * gram
            except:
                return 0
        else:
            quan = quan.split("~")
            suji = re.search("\d+",quan[0])
            quan[0] = suji.group()
            suji = re.search("\d+",quan[1])
            try:
                quan[1] = suji.group()
            except:
                return float(quan[0]) * gram
            return ((float(quan[0]) + float(quan[1])) / 2) * gram
    else:
        quan = quan.split("/")
        suji = re.search("\d+",quan[0])
        quan[0] = suji.group()           
        suji = re.search("\d+",quan[1])
        quan[1] = suji.group()
        return (float(quan[0]) / float(quan[1])) * float(gram)



"""食材ID辞書と食品グラム変換辞書を用いて食材の単位とそのgを取り出す"""
def Match_gram(ID,dict_ID):
    if ID == -1:
        #print shok + "は辞書に登録されていません\n"
        return 0
    dict_gram = make_dict("Dict_Foodgram")
    if ID in dict_gram:
        unit = dict_gram[ID][2]
        gram = float(dict_gram[ID][3])
        return [unit, gram]
    else:
        return 0


"""分量をgに変換する"""
def trans_gram(quan, unit, gram):
    if quan.find(unit) == -1:#単位が辞書と異なる場合は分量は０とする
        return 0
    else:
        quan = quan.rsplit(unit, 1)[0]
        quan = unicodedata.normalize('NFKC', quan.decode("utf-8"))
        quan = quan.encode("utf-8")
        if quan.find("/") == -1:
            if quan.find("~") == -1:
                suji = re.search("\d+",quan)
                quan = suji.group()
                return float(quan) * float(gram)
            else:
                quan = quan.split("~")
                suji = re.search("\d+",quan[0])
                quan[0] = suji.group()           
                suji = re.search("\d+",quan[1])
                quan[1] = suji.group()
                return ((float(quan[0]) + float(quan[1])) / 2) * float(gram)
        else:
            quan = quan.split("/")
            suji = re.search("\d+",quan[0])
            quan[0] = suji.group()           
            suji = re.search("\d+",quan[1])
            quan[1] = suji.group()
            return (float(quan[0]) / float(quan[1])) * float(gram)


"""辞書作成"""
def make_dict(Dict):
   f = open(Dict,"r")
   m = {}
   for line in f:
       cut = line.split(",")
       m[cut[0]] = cut
   f.close()
   return m


"""食材のIDを登録した辞書dict_IDを作成"""
def make_ID():
    f = open("Dict_FoodID_2","r")
    dict_ID = []
    for line in f:
        ID = []
        cut = line.split(",")
        for i in cut:
            if i.endswith("\n"):
                i = i.split("\n")[0]
            ID.append(i)
        dict_ID.append(ID)
    f.close()
    return dict_ID


"""辞書dict_IDを用いてshok(食材)のIDを検索"""
def Read_ID(shok, dict_ID):
    ret = []
    for i in dict_ID:
        for j in i:
            if shok == j:
                ret.append(i[0])
    return ret

"""食材名から食品IDを検索"""
def Search_ID(ID):
    f = open("Dict_FoodID_2","r")
    for line in f:
        if ID == line.split(",")[0]:
            return line.split(",")[1]


"""matelist(食材リスト)と食品群辞書を用いて分量を点数化、食品群を追加"""
def Read_group(matelist, dict_ID):
    re_matelist = []
    for mate in matelist:
        ID = mate[0]
        quan = mate[1]
        try:
            kon = Match_group(ID, dict_ID)#IDが指す食品の小分類と廃棄率と1点あたりのカロリーを取り出す
            group = kon[0]#group:小分類（肉類とか）
            reject = kon[1]#reject:廃棄率
            cal = kon[2]#cal:1点あたりのカロリー
            quan = trans_use(quan, reject, cal)#4群点数法の点数に変換
            re_mate = [ID,quan,group]
            re_matelist.append(re_mate)
        except:
            pass
    return re_matelist


"""食材、食材ID辞書、食品群辞書を用いて食品の群、廃棄率、1点あたりのcalを取り出す"""
def Match_group(ID, dict_ID):
    if ID == -1:
        #print shok + "は辞書に登録されていません\n"
        return 0
    dict_group = make_dict("Dict_FoodGroup")
    if ID in dict_group:
        group = dict_group[ID][4]
        reject = float(dict_group[ID][2])
        cal = float(dict_group[ID][3])
        return [group,reject,cal]
    else:
        return 0


"""4群点数法の点数化"""
def trans_use(quan, reject, cal):
    return (float(quan)*((100-reject)/100))/cal


"""食材リストから指定された類に分類し点数を加算していく"""
def Read_value(matelist, survfor):
    value = {"乳類":0, "卵類":0, "魚介類":0, "肉類":0, "豆類":0, "野菜類":0,
             "いも類":0, "果実類":0, "穀類":0, "砂糖類":0, "油脂類":0}
    for mate in matelist:
        value[mate[2]] = value[mate[2]] + float(mate[1])/survfor
    return value


def Trans_group(value):
    re_value = {}
    re_value["第1群"] = value["乳類"] + value["卵類"]
    re_value["第2群"] = value["魚介類"] + value["肉類"] + value["豆類"]
    re_value["第3群"] = value["野菜類"] + value["いも類"] + value["果実類"]
    re_value["第4群"] = value["穀類"] + value["砂糖類"] + value["油脂類"]
    return re_value


"""何人分か確認"""
def Read_survfor(survfor):
    try:
        if survfor.endswith("人分"):
            survfor = unicodedata.normalize('NFKC', survfor.split("人分")[0].decode("utf-8"))
            survfor = survfor.encode("utf-8")
        elif survfor.endswith("人前"):
            survfor = unicodedata.normalize('NFKC', survfor.split("人前")[0].decode("utf-8"))
            survfor = survfor.encode("utf-8")
        else:
            return 3.5
        if survfor.find("~") == -1:
            return float(survfor)
        else:
            return (float(survfor.split("~")[0]) + float(survfor.split("~")[1])) / 2
    except:
        return 3.5

 
"""２つのレシピの点数を加算する"""
def valueplus(value, Max_value):
    value_ret = {}
    for i in Max_value.keys():
        value_ret[i] = value[i] + Max_value[i]
    return value_ret

"""関連度を計算する（制限あり）"""
def cast_kanrendo(value, value_sub):
    kanrendo = 0
    for title, mate in value_sub:
        value2 = valueplus(value, mate)
        mm = math.fabs(float(3 - math.fabs(3 - value2["第1群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第2群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第3群"])))/3
        if value2["第1群"] > 6 or value2["第2群"] > 6 or value2["第3群"] > 6 or value2["第4群"] > 11:
            pass
        elif mm > kanrendo:
            kanrendo = mm
            valueaccept = mate
            titleaccept = title
    return titleaccept, valueaccept, kanrendo

"""関連度を計算する（制限なし）"""
def cast_kanrendo22(value, value_sub):
    kanrendo = 0
    for title, mate in value_sub:
        value2 = valueplus(value, mate)
        mm = math.fabs(float(3 - math.fabs(3 - value2["第1群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第2群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第3群"])))/3 + math.fabs(float(11 - math.fabs(11 - value2["第4群"])))/11
        if mm > kanrendo:
            kanrendo = mm
            valueaccept = mate
            titleaccept = title
    return titleaccept, valueaccept, kanrendo


"""4群を表示する"""
def print_value(value):
    print "第1群：", value["第1群"]
    print "第2群：", value["第2群"]
    print "第3群：", value["第3群"]
    print "第4群：", value["第4群"]
    return


matelist = Read_mate("recipedata_main1")

a = input("主菜を決めて下さい：")
num = int(a) - 1

dict_ID = make_ID()
matelist_sub = Read_mate("recipe_test2")

#主菜レシピの解析（選んだレシピを１つだけ解析）
re_matelist = matelist[num][2]
survfor = Read_survfor(matelist[num][1])#何人分か確認
re_matelist = Macth_ID(re_matelist, dict_ID)#食品ID辞書を用いて食材を特定
re_matelist = Read_gram(re_matelist, dict_ID)#食品グラム変換辞書を用いてグラム変換
re_matelist = Read_group(re_matelist, dict_ID)#食品群辞書を用いて4群点数法の点数に変換
value = Read_value(re_matelist, survfor)#人数分で割って1人分の栄養素を計算
value = Trans_group(value)#４群に変換（変換前は野菜類とか肉類とか小分類されてます）


#副菜レシピの解析（副菜レシピをすべて解析）
value_sub = []
for k in matelist_sub:
    re_matelist_sub = k[2]
    survfor_sub = Read_survfor(k[1])
    re_matelist_sub = Macth_ID(re_matelist_sub, dict_ID)
    re_matelist_sub = Read_gram(re_matelist_sub, dict_ID)
    re_matelist_sub = Read_group(re_matelist_sub, dict_ID)
    valuesub = Read_value(re_matelist_sub, survfor_sub)
    valuesub = Trans_group(valuesub)
    value_sub.append([k[0], valuesub])


print "選ばれたレシピ：" + matelist[num][0]
print "主菜"
print_value(value)

#制限ありの関連度か制限なしの関連度か選択（実験は制限ありでやってます。１です。）
flag = sys.argv[1]
if flag == "1":
    titleaccept, valueaccept, kanrendo = cast_kanrendo(value, value_sub)
elif flag == "2":
    titleaccept, valueaccept, kanrendo = cast_kanrendo22(value, value_sub)

#結果を表示
print "\n関連度：", kanrendo
print "\n関連度がもっとも高いレシピ：", titleaccept
print_value(valueaccept)

print "\n合計した栄養バランス"
value = valueplus(value, valueaccept)
print_value(value)

