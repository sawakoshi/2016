
def cast(k):
    f = open(k, "r")
    for line in f:
        if line.startswith("1:"):
            count1 = line.split("1:")[1]            
        if line.startswith("2:"):
            count2 = line.split("2:")[1]
        if line.startswith("3:"):
            count3 = line.split("3:")[1]
        if line.startswith("4:"):
            count4 = line.split("4:")[1]
    f.close()
    return float(count1), float(count2), float(count3), float(count4)


a1, b1, c1, d1 = cast("recipedata_kondate601")
a2, b2, c2, d2 = cast("recipedata_kondate670")
a3, b3, c3, d3 = cast("recipedata_kondate801")
a4, b4, c4, d4 = cast("recipedata_kondate802")
a5, b5, c5, d5 = cast("recipedata_kondate803")
a6, b6, c6, d6 = cast("recipedata_kondate807")
a7, b7, c7, d7 = cast("recipedata_kondate810")
a8, b8, c8, d8 = cast("recipedata_kondate811")
a9, b9, c9, d9 = cast("recipedata_kondate813")
a10, b10, c10, d10 = cast("recipedata_kondate814")

a = a1+a2+a3+a4+a5+a6+a7+a8+a9+a10
b = b1+b2+b3+b4+b5+b6+b7+b8+b9+b10
c = c1+c2+c3+c4+c5+c6+c7+c8+c9+c10
d = d1+d2+d3+d4+d5+d6+d7+d8+d9+d10

print a/10, b/10, c/10, d/10
 
