# -*- coding: utf-8 -*-
import unicodedata
import MeCab
import math
import sys
import re

mecab = MeCab.Tagger("Ochasen")

"""レシピから食材リストを作成"""
def Read_mate(recipe):
    f = open(recipe,"r")
    matelist = []
    for line in f:
        mate = []
        try:
            title = line.split("/", 3)[0]
            survfor = line.split("/", 3)[2]
            line = line.split("/", 3)[3]
        except:
            title = "なし"
            survfor = "なし"
        for shok in line.split("<>"):
            if shok != "\n" and shok != "":
                mate.append(shok)
        matelist.append([title, survfor, mate])
    f.close()
    return matelist


def Macth_ID(matelist, dict_ID):
    re_matelist = []
    count = 0
    count_ok = 0
    count_ng = 0
    count_ngok = 0
    for mate in matelist:
        shok = mate.split(",")[0]
        try:
            quan = mate.split(",")[1]
        except:
            quan = "0"
        count += 1
        if shok.find("（") != -1:
            shok = shok.split("（")[0]
        elif shok.find("(") != -1:
            shok = shok.split("(")[0]
        ID = Read_ID(shok, dict_ID)
        if len(ID) == 0:
            classify = {}
            count_ng += 1
            node = mecab.parseToNode(shok)
            while node:
                meta = node.feature.split(",")
                word = node.surface
                if meta[0] == "名詞":
                    ID = Read_ID(word, dict_ID)
                    if len(ID) != 0:
                        for i in ID:
                            if not i in classify:
                                classify[i] = 1
                            else:
                                classify[i] += 1
                node = node.next
            try:
                ID = max([(v,k) for k,v in classify.items()])[1]
                shok = Search_ID(ID)
                count_ngok += 1
            except:
                pass
        else:
            classify = {}
            for i in ID:
                if not i in classify:
                    classify[i] = 1
                else:
                    classify[i] += 1
            ID = max([(v,k) for k,v in classify.items()])[1]
            shok = Search_ID(ID)
            count_ok += 1
        if len(ID) != 0:
            re_matelist.append(ID + "," + quan)
    return re_matelist, [count, count_ok, count_ng, count_ngok]

 


"""食材リストと食品グラム変換辞書を用いて分量をgに変換する"""
def Read_gram(matelist, dict_ID):
    re_matelist = []
    for mate in matelist:
        ID = mate.split(",")[0]
        try:
            quan = mate.split(",")[1]
        except:
            quan = "0"
        flag_gram = check_gram(quan)
        flag_cup = check_cup(ID, quan)
        if flag_cup != 0:
           try:
                re_mate = [ID,flag_cup]
                re_matelist.append(re_mate)
           except:
                pass
        elif flag_gram != 0:
            re_mate = [ID,flag_gram]
            re_matelist.append(re_mate)
        else:
            try:
                kon = Match_gram(ID, dict_ID)
                if kon == 0:
                    quan = 0
                else:
                    unit = kon[0]
                    gram = kon[1]
                    quan = trans_gram(quan, unit, gram)
                re_mate = [ID, quan]
                re_matelist.append(re_mate)
            except:
                pass
    return re_matelist


"""分量が◯◯gと表記されているか確認。書かれていた場合は◯◯の部分を取り出す"""
def check_gram(quan):
    if quan.find("g") != -1 or quan.find("ｇ") != -1 or quan.find("グラム") != -1:
        quan = unicodedata.normalize('NFKC', quan.decode("utf-8"))
        quan = quan.encode("utf-8")
    else:
        return 0
    if quan.find("~") == -1:
        suji = re.findall("\d+",quan)
        quan = suji[-1]
        return quan
    else:
        quan = quan.split("~")
        suji = re.search("\d+",quan[0])
        quan[0] = suji.group()           
        suji = re.search("\d+",quan[1])
        try:
            quan[1] = suji.group()
        except:
            return float(quan[0])
        try:
            return (float(quan[0]) + float(quan[1])) / 2
        except:
            return 0


"""分量が大さじ小さじカップでかかれているか確認。"""
def check_cup(ID, quan):
    dict_gramcup = make_dict("Dict_Foodgram_cup_ver2")
    if ID in dict_gramcup:
        if quan.startswith("大さじ") or quan.startswith("大匙") or quan.startswith("大"):
            gram = float(dict_gramcup[ID][3])
        elif quan.startswith("小さじ") or quan.startswith("小匙") or quan.startswith("小"):
            gram = float(dict_gramcup[ID][2])
        elif quan.find("マグカップ") != -1:
            return 0
        elif quan.find("カップ") != -1:
            gram = float(dict_gramcup[ID][4])
        elif quan.find("ml") != -1 or quan.find("cc") != -1:
            gram = float(dict_gramcup[ID][4]) / 200
        else:
            return 0
    else:
        return 0
    quan = unicodedata.normalize('NFKC', quan.decode("utf-8"))
    quan = quan.encode("utf-8")
    if quan.find("~") == -1:
        suji = re.search("\d+",quan)
        try:
            quan = suji.group()
            return  float(quan) * gram
        except:
            return 0
    else:
        quan = quan.split("~")
        suji = re.search("\d+",quan[0])
        quan[0] = suji.group()
        suji = re.search("\d+",quan[1])
        try:
            quan[1] = suji.group()
        except:
            return float(quan[0]) * gram
        return ((float(quan[0]) + float(quan[1])) / 2) * gram
 

"""食材ID辞書と食品グラム変換辞書を用いて食材の単位とそのgを取り出す"""
def Match_gram(ID,dict_ID):
    if ID == -1:
        #print shok + "は辞書に登録されていません\n"
        return 0
    dict_gram = make_dict("Dict_Foodgram_ver2")
    if ID in dict_gram:
        unit = dict_gram[ID][2]
        gram = float(dict_gram[ID][3])
        return [unit, gram]
    else:
        return 0


"""分量をgに変換する"""
def trans_gram(quan, unit2, gram):
    unit2 = unit2.split(",")
    unit = "0"
    for i in unit2:
        if quan.find(i) != -1:
            unit = i
    if unit == "0":
        return 0
    else:
        quan = quan.rsplit(unit, 1)[0]
        quan = unicodedata.normalize('NFKC', quan.decode("utf-8"))
        quan = quan.encode("utf-8")
        if quan.find("/") == -1:
            if quan.find("~") == -1:
                suji = re.findall("\d+",quan)
                quan = suji[-1]
                return float(quan) * float(gram)
            else:
                quan = quan.split("~")
                suji = re.search("\d+",quan[0])
                quan[0] = suji.group()           
                suji = re.search("\d+",quan[1])
                quan[1] = suji.group()
                return ((float(quan[0]) + float(quan[1])) / 2) * float(gram)
        else:
            quan = quan.split("/")
            suji = re.search("\d+",quan[0])
            quan[0] = suji.group()           
            suji = re.search("\d+",quan[1])
            quan[1] = suji.group()
            return (float(quan[0]) / float(quan[1])) * float(gram)


"""辞書作成"""
def make_dict(Dict):
   f = open(Dict,"r")
   m = {}
   for line in f:
       cut = line.split("/")
       m[cut[0]] = cut
   f.close()
   return m


"""食材のIDを登録した辞書dict_IDを作成"""
def make_ID():
    f = open("Dict_FoodID_ver3_2","r")
    dict_ID = []
    for line in f:
        ID = []
        cut = line.split("/")
        for i in cut:
            if i.endswith("\n"):
                i = i.split("\n")[0]
            ID.append(i)
        dict_ID.append(ID)
    f.close()
    return dict_ID


"""辞書dict_IDを用いてshok(食材)のIDを検索"""
def Read_ID(shok, dict_ID):
    ret = []
    for i in dict_ID:
        mate = i[2].split(",")
        for j in mate:
            if shok == j:
                ret.append(i[0])
    return ret

def Search_ID(ID):
    f = open("Dict_FoodID_ver3_2","r")
    for line in f:
        if ID == line.split("/")[0]:
            return line.split("/")[1]


"""matelist(食材リスト)と食品群辞書を用いて分量を点数化、食品群を追加"""
def Read_group(matelist, dict_ID):
    re_matelist = []
    for mate in matelist:
        ID = mate[0]
        quan = mate[1]
        try:
            kon = Match_group(ID, dict_ID)
            group = kon[0]
            reject = kon[1]
            cal = kon[2]
            quan = trans_use(quan, reject, cal)
            re_mate = [ID,quan,group]
            re_matelist.append(re_mate)
        except:
            pass
    return re_matelist


"""食材、食材ID辞書、食品群辞書を用いて食品の群、廃棄率、1点あたりのcalを取り出す"""
def Match_group(ID, dict_ID):
    if ID == -1:
        #print shok + "は辞書に登録されていません\n"
        return 0
    dict_group = make_dict("Dict_FoodGroup_ver2")
    if ID in dict_group:
        group = dict_group[ID][4]
        reject = float(dict_group[ID][2])
        cal = float(dict_group[ID][3])
        return [group,reject,cal]
    else:
        return 0


"""4群点数法の点数化"""
def trans_use(quan, reject, cal):
    return (float(quan)*((100-reject)/100))/cal


"""食材リストから指定された類に分類し点数を加算していく"""
def Read_value(matelist, survfor):
    value = {"乳類":0, "卵類":0, "魚介類":0, "肉類":0, "豆類":0, "野菜類":0,
             "いも類":0, "果実類":0, "穀類":0, "砂糖類":0, "油脂類":0}
    for mate in matelist:
        value[mate[2]] = value[mate[2]] + float(mate[1])/survfor
    return value


def Trans_group(value):
    re_value = {}
    re_value["第1群"] = value["乳類"] + value["卵類"]
    re_value["第2群"] = value["魚介類"] + value["肉類"] + value["豆類"]
    re_value["第3群"] = value["野菜類"] + value["いも類"] + value["果実類"]
    re_value["第4群"] = value["穀類"] + value["砂糖類"] + value["油脂類"]
    return re_value


"""何人分か確認"""
def Read_survfor(survfor):
    try:
        if survfor.find("人") != -1:
            survfor = unicodedata.normalize('NFKC', survfor.split("人")[0].decode("utf-8"))
            survfor = survfor.encode("utf-8")
        elif survfor.endswith("人前"):
            survfor = unicodedata.normalize('NFKC', survfor.split("人前")[0].decode("utf-8"))
            survfor = survfor.encode("utf-8")
        else:
            return 0
        if survfor.find("~") == -1:
            try:
                return float(survfor)
            except:
                return 0
        else:
            try:
                return (float(survfor.split("~")[0]) + float(survfor.split("~")[1])) / 2
            except:
                return 0
    except:
        return 0

 
def Min_composition(value):
    return min(value, key=(lambda x: value[x]))


def Max_composition(value_sub, Min_composition):
    Max = 0
    Max_value = {}
    for i in value_sub:
        if i[Min_composition] > Max:
            Max = i[Min_composition]
            Max_value = i
    return Max_value


def valueplus(value, Max_value):
    value_ret = {}
    for i in Max_value.keys():
        value_ret[i] = value[i] + Max_value[i]
    return value_ret

def cast_kanrendo(value, value_sub):
    kanrendo = 0
    for title, mate in value_sub:
        value2 = valueplus(value, mate)
        mm = math.fabs(float(3 - math.fabs(3 - value2["第1群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第2群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第3群"])))/3
        if value2["第1群"] > 6 or value2["第2群"] > 6 or value2["第3群"] > 6 or value2["第4群"] > 11:
            pass
        elif mm > kanrendo:
            kanrendo = mm
            valueaccept = mate
            titleaccept = title
    return titleaccept, valueaccept, kanrendo

def cast_kanrendo22(value, value_sub):
    kanrendo = 0
    for title, mate in value_sub:
        value2 = valueplus(value, mate)
        mm = math.fabs(float(3 - math.fabs(3 - value2["第1群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第2群"])))/3 + math.fabs(float(3 - math.fabs(3 - value2["第3群"])))/3
        if mm > kanrendo:
            kanrendo = mm
            valueaccept = mate
            titleaccept = title
    return titleaccept, valueaccept, kanrendo



def print_value(value):
    print "第1群：", value["第1群"]
    print "第2群：", value["第2群"]
    print "第3群：", value["第3群"]
    print "第4群：", value["第4群"]
    return

a = sys.argv[1]
dict_ID = make_ID()
matelist_sub = Read_mate("kondate" + a)
surv = matelist_sub[0][1]
surv = Read_survfor(surv)
value = {}
value["第1群"] = 0
value["第2群"] = 0
value["第3群"] = 0
value["第4群"] = 0
print surv
value_sub = []
for k in matelist_sub:
    re_matelist_sub = k[2]
    survfor_sub = Read_survfor(k[1])
    if survfor_sub == 0:
        survfor_sub = surv
    re_matelist_sub, counter = Macth_ID(re_matelist_sub, dict_ID)
    re_matelist_sub = Read_gram(re_matelist_sub, dict_ID)
    re_matelist_sub = Read_group(re_matelist_sub, dict_ID)
    for i in re_matelist_sub:
        print Search_ID(i[0]), i[1]
    valuesub = Read_value(re_matelist_sub, survfor_sub)
    valuesub = Trans_group(valuesub)
    value = valueplus(value, valuesub)
    value_sub.append([k[0], valuesub])

print_value(value)
